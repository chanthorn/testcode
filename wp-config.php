<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress1' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'VpfhxCvM0VYh$=N1d ?D%zrhDEpE[UjjIGpjEsWnE8nl>b<&<rANam|2^gDzJamB' );
define( 'SECURE_AUTH_KEY',  '~!V=!bl~SY2wiMC8O*F..iTq-=OWK`-~oj&lh19 nLGB&>!FKXvN(@nl7L%0$37K' );
define( 'LOGGED_IN_KEY',    'OVS!7.i6^&7!I[w6Wi-V%4.G }?!{:*0+u2!/{GG~/CuR`3o{q2*]Pq:h%Y1t:wq' );
define( 'NONCE_KEY',        '=75;:[[aZh1vcN#xfg`DQ&k^m.z@gd(m<ub3Sa!r[A7g:H8}<b-aAI1BO+~07{Q)' );
define( 'AUTH_SALT',        '<eEYERAp$]nKLwz?xWn8tpo*12M,0R94O?|DWD5I(c%lPt^@CW6eGUD]D/Avku~r' );
define( 'SECURE_AUTH_SALT', 'f)eAhePu0>%MHD/tN;8om66*IC)_]vC264c6[N!Kch2;?L$N;M8>o:F[liX}}s-%' );
define( 'LOGGED_IN_SALT',   ',g#`VSV@P&N8QW@6DKtw8 K@B|*ucSDVg|D=$Iudh;h=jO<%R}s`Mom|Vh*8;F<:' );
define( 'NONCE_SALT',       '@F3>[#xzW6;9nMoD`*K#Wu#6|Z`fa&rvPvv7WX@%gG`5Z>(Om4<gVJfc)tqgmh[6' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
